// Pylon.cpp : Defines the entry point for the console application.
//
/*
*This is used to bridge the gap between subnets.
*It will act as beacon for the server / data source(i.e.X - Plane) to send the data over
*It receives data from server and then forward it to the multicast address for all the Helper
*to read it.
*/
#include <winsock2.h>
#include <Ws2tcpip.h>
#include <iostream>
#include "stdafx.h"
#include "Data.h"
#pragma comment(lib,"ws2_32.lib")
int iResult;
WSADATA wsaData;
SOCKET dispatchSocket, receiveSocket;
sockaddr_in receiveAddress, multicastAddress;
int receiveAddressSize = sizeof(receiveAddress);
int multicastAddressSize = sizeof(multicastAddress);

int startReceiver() {
	// Initialize Winsock
	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != NO_ERROR) {
		std::cout << "WSAStartup for UDP failed with error " << iResult << "\n";
	}

	//Create dispatch socket
	if ((dispatchSocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == SOCKET_ERROR)
	{
		std::cout << "dispatchSocket create failed with error code:" << WSAGetLastError() << "\n";
	}

	//Create a receiver socket to receive datagrams
	receiveSocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (receiveSocket == INVALID_SOCKET) {
		std::cout << "receiveSocket create failed with error: " << WSAGetLastError() << "\n";
	}

	receiveAddress.sin_family = AF_INET;
	receiveAddress.sin_port = htons(8888);
	receiveAddress.sin_addr.s_addr = htonl(INADDR_ANY);

	//Set address structure
	multicastAddress.sin_family = AF_INET;
	multicastAddress.sin_port = htons(8800);
	//The address 239.0.0.0 will be reserved for Boeing multicast group
	//Will need to create table for each system, ie 737, 777, helicopter
	InetPton(AF_INET, _T("239.0.0.0"), &multicastAddress.sin_addr.s_addr);
	// Bind the socket to any address and the specified port.
	iResult = bind(receiveSocket, (SOCKADDR *)& receiveAddress, sizeof (receiveAddress));
	if (iResult != 0) {
		std::cout << "receiveSocket bind failed with error: " << WSAGetLastError() << "\n";
	}

	return 1;
}
int main() {
	if (startReceiver()) {
		std::cout << "Pylon is up and running." << "\n";

		while (1) {
			if (recvfrom(receiveSocket, (char*)&forwardMessage, sizeof(forwardMessage), 0, (SOCKADDR *)& receiveAddress, &receiveAddressSize) > 0) {
				//printf("Airspeed: %f\n", forwardMessage.Airspeed);
				if (sendto(dispatchSocket, (char*)&forwardMessage, sizeof(forwardMessage), 0, (SOCKADDR *)&multicastAddress, multicastAddressSize) < 0) {
					std::cout << "Sent fail: " << WSAGetLastError() << "\n";
				}
			}
		}
	}
	else {
		std::cout << "Cannot initialise Pylon." << "\n";
		return 1;
	}
}
